package de.tantalos.inventorygui.api;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Set;

import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.Plugin;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;

import net.md_5.bungee.api.ChatColor;

public class BungeeTeleportButton extends Button {

    private final String serverName;
    private final Plugin plugin;

    private final static Set<Plugin> channelPlugins = Sets.newHashSet();

    public BungeeTeleportButton(Plugin plugin, String serverName, ItemStack itemStack) {
        super(itemStack);
        if (!channelPlugins.contains(plugin)) {
            plugin.getServer().getMessenger().registerOutgoingPluginChannel(plugin, "BungeeCord");
            plugin.getLogger().info("Plugin channel registered");
        }

        Preconditions.checkNotNull(serverName);
        Preconditions.checkArgument(!serverName.contains(" "), "Server name must be a string without ' ' ");
        Preconditions.checkArgument(serverName.matches("[a-zA-Z0-9_]*"), "Server name must only use [a-zA-Z0-9_]");
        this.serverName = serverName;
        this.plugin = plugin;
    }

    @Override
    public void onButtonClicked(HumanEntity player) {
        if (player == null || !(player instanceof Player)) {
            return;
        }
        Player mcPlayer = (Player) player;


        if (!hasPermission(mcPlayer)) {
            mcPlayer.sendMessage(ChatColor.RED + "You don't have permission");
            return;
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(baos);
        try {
            dos.writeUTF("Connect");
            dos.writeUTF(serverName);
            mcPlayer.sendPluginMessage(plugin, "BungeeCord", baos.toByteArray());
            baos.close();
            dos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean hasPermission(Player mcPlayer) {
        Permission specificPermission = new Permission("inventorygui.button.bungee.teleport." + serverName);

        return mcPlayer.isPermissionSet(specificPermission);
    }
}
