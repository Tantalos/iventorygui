package de.tantalos.inventorygui.api;

public class IllegalPatternException extends RuntimeException {

    private static final long serialVersionUID = -5250816023998062238L;

    public IllegalPatternException() {
        super();
    }

    public IllegalPatternException(String msg) {
        super(msg);
    }

    public IllegalPatternException(Throwable cause, String msg) {
        super(msg, cause);
    }
}
