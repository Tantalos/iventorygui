package de.tantalos.inventorygui.api;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.tantalos.itemset.connector.ItemBuilder;

public abstract class Button implements ItemBuilder {
	
    @SuppressWarnings("deprecation")
    private final static ItemStack BLACK_BUTTON = new ItemStack(Material.STAINED_GLASS_PANE, 1,
            DyeColor.BLACK.getWoolData());
    {
        ItemMeta meta = BLACK_BUTTON.getItemMeta();
        meta.setDisplayName(" ");
        BLACK_BUTTON.setItemMeta(meta);
    }

    public static Button getNothing(Material material) {
    	ItemStack item = new ItemStack(material);
    	return new Button(item) {

            @Override
            public void onButtonClicked(HumanEntity player) {
            }
        };
    }
    
    public static Button getNothing(ItemStack item) {
    	return new Button(item) {

            @Override
            public void onButtonClicked(HumanEntity player) {
            }
        };
    }
    
    public final static Button NOTHING = getNothing(BLACK_BUTTON);
    


    private final ItemStack guiElement;

    public Button(ItemStack guiElement) {
        this.guiElement = guiElement;
    }

    public abstract void onButtonClicked(HumanEntity player);

    public ItemStack getGuiElement() {
        return guiElement;
    }

    @Override
    public ItemStack getItem() {
        return getGuiElement();
    }

}
