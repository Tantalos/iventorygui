package de.tantalos.inventorygui.api;

import org.bukkit.Location;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.Permission;

import net.md_5.bungee.api.ChatColor;

public class TeleportButton extends Button {

    private final Location location;

    public TeleportButton(Location location, ItemStack itemStack) {
        super(itemStack);
        this.location = location;
    }

    @Override
    public void onButtonClicked(HumanEntity player) {
        if (!(player instanceof Player)) {
            return;
        }
        Player mcPlayer = (Player) player;
        if (!hasPermission(mcPlayer)) {
            mcPlayer.sendMessage(ChatColor.RED + "You don't have permission");
            return;
        }
        player.teleport(location);
    }

    private boolean hasPermission(Player mcPlayer) {
        Permission specificPermission = new Permission("inventorygui.button.teleport." + location.getWorld().getName());

        return mcPlayer.hasPermission(specificPermission);
    }
}
