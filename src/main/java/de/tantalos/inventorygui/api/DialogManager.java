package de.tantalos.inventorygui.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import com.google.common.base.Predicate;
import com.google.common.collect.Maps;

import de.tantalos.eventlistener.EventCallback;
import de.tantalos.eventlistener.InventoryClickEventListener;
import net.md_5.bungee.api.ChatColor;


public class DialogManager {
    private final ButtonListener INSTANCE = new ButtonListener();

    private final List<Button> buttons;
    private final HashMap<Integer, Button> buttonSlot;
    private final Plugin plugin;
    private final String arrangementPattern;
    private final Button placeholderButton;
    private final Inventory inventory;
    private final String inventoryTitle;

    DialogManager(Plugin plugin, String arrangementPattern, List<Button> buttons,
            Button placeholderButton, String inventoryTitle) {
        this.plugin = plugin;
        this.buttons = new ArrayList<Button>(buttons);
        this.buttonSlot = Maps.newHashMap();
        this.placeholderButton = placeholderButton;
        this.inventoryTitle = inventoryTitle;
        createListener(plugin);

        if (!VerifyUtil.verifyPattern(arrangementPattern)) {
            throw new IllegalPatternException(
                    "Inventory dialog pattern has to " + "have at least 9 characters, must be a multiple of 9 and must "
                            + "only use X, P and O character");
        }
        this.arrangementPattern = arrangementPattern.toUpperCase().replace(" ", "");
        this.inventory = buildInventory();
    }

    private InventoryClickEventListener createListener(Plugin plugin) {
        Predicate<InventoryClickEvent> buttonPredicate = new Predicate<InventoryClickEvent>() {

            @Override
            public boolean apply(InventoryClickEvent input) {
                if (input.getCurrentItem() == null) {
                    return false;
                }
                return true;
            }
        };
        return new InventoryClickEventListener(plugin, buttonPredicate,
                INSTANCE);
    }

    /**
     * Opens the inventory dialog for a player.
     *
     * @param player
     *            the player who will see the dialog
     */
    public void open(Player player) {
        if (!player.hasPermission("inventorygui.dialog.show")) {
            player.sendMessage(ChatColor.RED + "You don't have permission");
            return;
        }
        player.openInventory(inventory);
    }

    private Inventory buildInventory() {
        int size = arrangementPattern.length();
        plugin.getLogger().info("GUI dialog created with size: " + size);
        Inventory inventory = Bukkit.createInventory(null, size, inventoryTitle);

        // adds the buttons after the pattern
        int buttonCount = 0;
        int inventoryPosition = 0;
        while (inventoryPosition < arrangementPattern.length()) {
            if (arrangementPattern.charAt(inventoryPosition) == 'X') {
                if (buttonCount < buttons.size()) {
                    Button button = buttons.get(buttonCount);
                    inventory.setItem(inventoryPosition, button.getGuiElement());
                    buttonSlot.put(inventoryPosition, button);

                    buttonCount++;
                } else {
                    plugin.getLogger()
                            .warning("Pattern in gui dialog demands a button but no button more button specified");
                }
            } else if (arrangementPattern.charAt(inventoryPosition) == 'P') {
                inventory.setItem(inventoryPosition, placeholderButton.getGuiElement());
                buttonSlot.put(inventoryPosition, placeholderButton);
            } else if (arrangementPattern.charAt(inventoryPosition) == 'O') {
                // do nothing
            }
            inventoryPosition++;
        }
        if (buttonCount + 1 < buttons.size()) {
            plugin.getLogger().warning(
                    "not every button which was specified was inserted to the gui dialog. The pattern needs more X");
        }
        return inventory;
    }

    private class ButtonListener implements
            EventCallback<InventoryClickEvent> {
        @Override
        public void onEvent(InventoryClickEvent event) {
            Inventory inventory = event.getClickedInventory();
            if (!DialogManager.this.inventory.equals(inventory)) {
                return;
            }
            event.setCancelled(true);
            event.setResult(Result.DENY);
            ItemStack clickedItem = event.getCurrentItem();
            if (inventory == null && clickedItem == null) {
                return;
            }
            HumanEntity player = event.getWhoClicked();

            int slot = event.getSlot();
            Button button = buttonSlot.get(slot);
            if (button == null) {
                return;
            }
            button.onButtonClicked(player);
        }
    }
}
