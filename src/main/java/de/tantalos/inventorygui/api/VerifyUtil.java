package de.tantalos.inventorygui.api;

public class VerifyUtil {
    private static final int INVENTORY_MAX_SIZE = 54;

    public static boolean verifyPattern(String pattern) {
        if (pattern.length() > INVENTORY_MAX_SIZE) {
            return false;
        }
        if (pattern.length() == 0 || pattern.length() % 9 != 0) {
            return false;
        }

        pattern = pattern.toUpperCase();
        for (int i = 0; i < pattern.length(); i++) {
            char c = pattern.charAt(i);
            if (!"XPO".contains(String.valueOf(c))) {
                return false;
            }
        }
        return true;
    }
}
