package de.tantalos.inventorygui.api;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import net.md_5.bungee.api.ChatColor;

public class CommandButton extends Button {

    private final String command;
    private final CommandSender author;

    public CommandButton(String command, Plugin plugin, CommandSender author, ItemStack itemStack) {
        super(itemStack);
        this.command = command;
        this.author = author;
    }

    @Override
    public void onButtonClicked(HumanEntity player) {
        if (!(player instanceof Player)) {
            return;
        }
        Player mcPlayer = (Player) player;
        if (!mcPlayer.hasPermission("inventorygui.button.command")) {
            mcPlayer.sendMessage(ChatColor.RED + "You don't have permission");
            return;
        }
        if (!Bukkit.dispatchCommand(author, command)) {
            author.sendMessage(ChatColor.RED + "Command '" + command
                    + "' couldn't be executed!");
            player.sendMessage(ChatColor.RED + "Command '" + command
                    + "' couldn't be executed!");
        }
    }

}
