package de.tantalos.inventorygui.api;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import com.google.common.base.Predicate;

import de.tantalos.eventlistener.EventCallback;
import de.tantalos.eventlistener.PlayerInteractEventListener;
import de.tantalos.eventlistener.utils.PredicateUtils;

public class GuiItem {

    public static ItemStack guiItem(Plugin plugin, Material material, DialogBuilder builder) {
        final ItemStack teleportGui = new ItemStack(material);

        return guiItem(plugin, teleportGui, builder);
    }

    /**
     * Creates a item which opens a dialog when the item gets clicked
     *
     * @param plugin
     * @param guiOpener
     * @param builder
     * @return
     */
    public static ItemStack guiItem(Plugin plugin, ItemStack guiOpener, DialogBuilder builder) {

        Predicate<PlayerInteractEvent> pred = PredicateUtils.getUniqueItemClickPredicate(guiOpener);
        final DialogManager dia = builder.build();

        // which is triggered on item click
        new PlayerInteractEventListener(plugin, pred, new EventCallback<PlayerInteractEvent>() {
            @Override
            public void onEvent(PlayerInteractEvent event) {
                Player player = event.getPlayer();
                if (player != null) {
                    dia.open(player);
                }
            }
        });
        return guiOpener;
    }
}
