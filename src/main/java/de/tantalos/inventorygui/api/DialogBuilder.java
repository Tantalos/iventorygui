package de.tantalos.inventorygui.api;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bukkit.entity.HumanEntity;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import com.google.common.collect.Lists;

public class DialogBuilder {
    private final Plugin plugin;
    private List<Button> buttons;
    private String arrangementPattern;
    private Button placeholderButton;
    private boolean closeAfterClick = true;
    private String inventoryTitle = "Inventory GUI";

    public DialogBuilder(Plugin plugin) {
        this.plugin = plugin;
        buttons = Lists.newArrayList();
        arrangementPattern = "XXXXXXXXX";

        withPlaceholderItem(Button.NOTHING);
    }

    /**
     * <p>
     * A pattern after which the buttons will be arranged. X means button is
     * placed at this location. O means no button is placed at this location. P
     * is a placeholder item. If more buttons then pattern fields are specified,
     * the pattern will be repeated.
     * </p>
     *
     * <p>
     * Every line has to have a length of 9. The string must only use X,P and O
     * characters. For example:
     * </p>
     *
     * <pre>
     * XXOOXOOXX
     * PPXXPXXPP
     * XXOOXOOOO
     * </pre>
     *
     * @param pattern
     *            the pattern after which the buttons are arranged
     * @throws IllegalPatternException
     *             if pattern does not conform right syntax. Every line has to
     *             have a length of 9. The string must only use X,P and O
     *             characters.
     */
    public DialogBuilder setButtonArrangementPattern(String pattern) {
        pattern = pattern.toUpperCase().replace(" ", "");
        if (!VerifyUtil.verifyPattern(pattern)) {
            throw new IllegalPatternException(
                    "Inventory dialog pattern has to " + "have at least 9 characters, must be a multiple of 9 and must "
                            + "only use X, P and O character");
        }
        this.arrangementPattern = pattern.toUpperCase();
        return this;
    }

    public DialogBuilder addButton(Button button) {
        buttons.add(button);
        // buttonOrder.add(item);
        return this;
    }

    @Deprecated
    public DialogBuilder addButton(ItemStack item, Button button, String... buttonText) {
        if (buttonText != null && buttonText.length <= 0) {
            return addButton(item, button);
        }
        ItemMeta meta = item.getItemMeta();
        if (buttonText.length >= 1) {
            meta.setDisplayName(buttonText[0]);
        }
        if (buttonText.length > 1) {
            ArrayList<String> lore = new ArrayList<String>();
            for (int i = 1; i < buttonText.length; i++) {
                lore.add(buttonText[i]);
            }
            meta.setLore(lore);
        }
        item.setItemMeta(meta);

        return addButton(item, button);
    }

    public DialogBuilder setInventoryTitle(String inventoryTitle) {
        this.inventoryTitle = inventoryTitle;
        return this;
    }

    public DialogBuilder closeAfterClick(boolean flag) {
        closeAfterClick = flag;
        return this;
    }

    /**
     * Removes a button from inventory dialog.
     *
     * @param item
     *            the item instance which will be removed from the inventory
     *            dialog
     * @return true if the button was removed, false if button not.
     */
    public DialogBuilder deleteButton(Button item) {
        if (item == placeholderButton) {
            return this;
        }
        buttons.remove(item);
        return this;
    }

    /**
     * Defines the default placeholder item. Default is black thin glass without
     * effect
     *
     * @param placeholderButton
     *            the item stack which will be the placeholder item
     */
    public DialogBuilder withPlaceholderItem(Button button) {
        this.placeholderButton = button;

        return this;
    }

    public DialogManager build() {
        if (closeAfterClick) {
            Iterator<Button> it = buttons.iterator();
            List<Button> decoratedButtons = new ArrayList<>();
            while (it.hasNext()) {
                Button button = it.next();

                // skip placeholder item.
                if (placeholderButton.equals(button)) {
                    decoratedButtons.add(button);
                    continue;
                }
                Button decoratedButton = button;
                Button closeAfterClickButton = decorateWithCloseAfterClick(decoratedButton);
                decoratedButtons.add(closeAfterClickButton);
            }
            buttons = decoratedButtons;
        }
        return new DialogManager(plugin, arrangementPattern, buttons, placeholderButton, inventoryTitle);
    }

    private Button decorateWithCloseAfterClick(final Button decoratedButton) {
        return new Button(decoratedButton.getGuiElement()) {
            @Override
            public void onButtonClicked(HumanEntity player) {
                decoratedButton.onButtonClicked(player);
                player.closeInventory();
            }
        };
    }
}
