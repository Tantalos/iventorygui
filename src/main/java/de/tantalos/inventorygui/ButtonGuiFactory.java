package de.tantalos.inventorygui;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.inventory.ItemStack;

import de.tantalos.inventorygui.api.BungeeTeleportButton;
import de.tantalos.inventorygui.api.Button;
import de.tantalos.inventorygui.api.CommandButton;
import de.tantalos.inventorygui.api.TeleportButton;
import de.tantalos.itemset.connector.IllegalConfigurationException;
import de.tantalos.itemset.connector.ItemContainerFactory;
import de.tantalos.itemset.connector.ItemFactory;
import de.tantalos.itemset.utils.ItemUtils;

//instance of container because button can contain new gui item. Not implemented yet
public class ButtonGuiFactory extends ItemContainerFactory<Button> {

    public ButtonGuiFactory(List<Button> itemPrototype) {
        super(itemPrototype);
    }

    @Override
    public Button buildItemDefinition(String... args) throws IllegalConfigurationException {
        Button button = Button.NOTHING;

        if (args != null) {
            for (String arg : args) {
                String key = ItemFactory.getKey(arg);
                String value = ItemFactory.getValue(arg);

                switch (key) {
                case "type":
                    switch (value) {
                    case "command":
                        button = buildCommandButton(args);
                        break;
                    case "teleport":
                        button = buildTeleportButton(args);
                        break;
                    case "bungeeTeleport":
                        button = buildBungeeTeleportButton(args);
                        break;
                    case "nothing":
                    	button = buildEmptyButton(args);
                    	break;
                    default:
                        throw new IllegalConfigurationException("Unknown Button class " + value
                                + "\n Note custom buttons and compound guis are not supported yet");
                    }

                    break;
                }
            }
        }

        return button;
    }

	private Button buildEmptyButton(String[] args) throws IllegalConfigurationException {
		Material buttonMaterial = Material.BARRIER;
        String line1 = null;
        String line2 = null;

		for (String arg : args) {
			String key = ItemFactory.getKey(arg);
			String value = ItemFactory.getValue(arg);

			switch (key) {
			case "material":
				buttonMaterial = Material.valueOf(value);
				break;
			case "line1":
            case "name":
				line1 = value;
				break;
			case "line2":
            case "lore":
				line2 = value;
				break;
			}
		}

		ItemStack guiElement = ItemUtils.createItem(buttonMaterial, line1, line2);
        return Button.getNothing(guiElement);
	}

    private Button buildBungeeTeleportButton(String[] args) throws IllegalConfigurationException {
        Material buttonMaterial = Material.BARRIER;
        String line1 = "";
        String line2 = "";
        String serverName = null;

        for (String arg : args) {
            String key = ItemFactory.getKey(arg);
            String value = ItemFactory.getValue(arg);

            switch (key) {
            case "server":
                serverName = value;
                break;
            case "material":
                buttonMaterial = Material.valueOf(value);
                break;
            case "line1":
            case "name":
                line1 = value;
                break;
            case "line2":
            case "lore":
                line2 = value;
                break;
            default:
                break;
            }
        }

        if (serverName == null) {
            throw new IllegalConfigurationException(
                    "target server arguments is missing for bungee teleport button! ");
        }

        ItemStack guiElement = ItemUtils.createItem(buttonMaterial, line1, line2);
        return new BungeeTeleportButton(Main.plugin, serverName, guiElement);

    }

    private Button buildTeleportButton(String[] args) throws IllegalConfigurationException {
        Material buttonMaterial = Material.BARRIER;
        String line1 = "";
        String line2 = "";
        String worldName = null;
        Double xCord = null;
        Double yCord = null;
        Double zCord = null;

        for (String arg : args) {
            String key = ItemFactory.getKey(arg);
            String value = ItemFactory.getValue(arg);

            switch (key) {
            case "world":
                worldName = value;
                break;
            case "x":
                xCord = Double.valueOf(value);
                break;
            case "y":
                yCord = Double.valueOf(value);
                break;
            case "z":
                zCord = Double.valueOf(value);
                break;
            case "material":
                buttonMaterial = Material.valueOf(value);
                break;
            case "line1":
            case "name":
                line1 = value;
                break;
            case "line2":
            case "lore":
                line2 = value;
                break;
            default:
                break;
            }
        }

        if (worldName == null || xCord == null || yCord == null || zCord == null) {
            String locationString = "(" + worldName + ", " + xCord + ", " + yCord + ", " + yCord + ")";
            throw new IllegalConfigurationException(
                    "location arguments are missing for teleport button! " + locationString);
        }

        World world = Main.plugin.getServer().getWorld(worldName);
        if (world == null) {
            throw new IllegalConfigurationException("Unknown world name " + worldName);
        }

        Location target = new Location(world, xCord, yCord, zCord);
        ItemStack guiElement = ItemUtils.createItem(buttonMaterial, line1, line2);
        return new TeleportButton(target, guiElement);
    }

    private Button buildCommandButton(String[] args) throws IllegalConfigurationException {
        Material buttonMaterial = Material.BARRIER;
        String line1 = "";
        String line2 = "";

        String command = null;

        for (String arg : args) {
            String key = ItemFactory.getKey(arg);
            String value = ItemFactory.getValue(arg);

            switch (key) {
            case "command":
                command = value;
                break;
            case "material":
                buttonMaterial = Material.valueOf(value);
                break;
            case "line1":
            case "name":
                line1 = value;
                break;
            case "line2":
            case "lore":
                line2 = value;
                break;
            }
        }

        if (command == null) {
            throw new IllegalConfigurationException("Command button needs a command defined after button type");
        }

        ItemStack guiElement = ItemUtils.createItem(buttonMaterial, line1, line2);
        return new CommandButton(command, Main.plugin, Main.plugin.getServer().getConsoleSender(), guiElement);
    }

}
