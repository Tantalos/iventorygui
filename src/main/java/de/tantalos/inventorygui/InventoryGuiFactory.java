package de.tantalos.inventorygui;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import de.tantalos.inventorygui.api.Button;
import de.tantalos.inventorygui.api.DialogBuilder;
import de.tantalos.inventorygui.api.GuiItem;
import de.tantalos.itemset.connector.IllegalConfigurationException;
import de.tantalos.itemset.connector.ItemBuilder;
import de.tantalos.itemset.connector.ItemContainerFactory;
import de.tantalos.itemset.connector.ItemFactory;
import de.tantalos.itemset.utils.ItemUtils;

public class InventoryGuiFactory extends ItemContainerFactory<Button> {


    public InventoryGuiFactory(List<Button> nestedButtons) {
        super(nestedButtons);
    }

    @SuppressWarnings("unchecked")
    @Override
    public ItemBuilder buildItemDefinition(String... args) throws IllegalConfigurationException {
        final DialogBuilder builder = new DialogBuilder(Main.plugin);
        Material guiMaterial = Material.COMPASS;
        String name = null;
        String lore = null;

        for (Button button : ((List<Button>) this.nestedItems)) {
            builder.addButton(button);
        }

        if (args != null) {
            for (String arg : args) {
                String key = ItemFactory.getKey(arg);
                String value = ItemFactory.getValue(arg);
                switch (key) {
                case "material":
                    guiMaterial = Material.valueOf(value);
                    break;
                case "pattern":
                    builder.setButtonArrangementPattern(value);
                    break;
                case "title":
                    builder.setInventoryTitle(value);
                    break;
                case "line1":
                case "name":
                    name = value;
                    break;
                case "line2":
                case "lore":
                    lore = value;
                    break;
                default:
                    throw new IllegalConfigurationException("Unknown parameter key: " + key);

                }
            }
        }

        final ItemStack guiDialogButton = ItemUtils.createItem(guiMaterial, name,
                lore);

        return (() -> {
            return GuiItem.guiItem(Main.plugin, guiDialogButton, builder);
        });
    }


}