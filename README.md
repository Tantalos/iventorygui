#Inventory GUI 

Inventory GUI is a library to create clickable items in inventory.

#How to use
To create an inventory gui dialog use DialogBuilder. You can configure your dialog using the set methods (e.g set title, set button arrangement, add buttons etc.).
After that you can build the Dialog.

```
#!Java

final DialogBuilder builder = new DialogBuilder(plugin);
builder.setTitle("Any Title");

// add clickable buttons
builder.addButton(button1);
builder.addButton(button2);
...

// build dialog and open for player
DialogManager dialog = builder.build();
dialog.open(player);

OR
// give item which opens dialog when used
GuiItem.guiItem(plugin, item, builder);
```


#Buttons
Generally a button contains an ItemStack (the look) and a method which is executed when clicked. Currently there are 3 predefined Buttons

- **TeleportButton** which teleports the player to the given location when clicked
- **CommandButton** which executes a minecraft command, when clicked. Command is executed in name of author
- **BungeeTeleportButton** which teleports the player to the given bungee server when clicked

```
#!Java

ItemStack look = new ItemStack(COMPASS);
Button button1 = new TeleportButton(location, look);
Button button2 = new CommandButton("a minecraft command", plugin, player, look);
Button button3 = new BungeeTeleportButton(plugin, "bungee server name", look);

```

#Custom Button
You can inherit from the abstract Button.class to create custom buttons. The `onButtonClicked(HumanEntity player)` method is called when button was clicked (player is the player who clicked).

```
#!Java

class MyButton extends Button {
    public MyButton(ItemStack itemStack) {
        super(itemStack);
    }

    @Override
    public void onButtonClicked(HumanEntity player) {
        // do your stuff when clicked
        ..
    }

}
```

##Button arrangement
You can define the arrangement of buttons in the dialog with a string with the length multiple of 9.

- X means place the button at this location
- P place a placeholder
- O keep this place empty

You can set the pattern in the dialog builder 

```
#!Java

builder.setButtonArrangementPattern("XPXPXPXPX" + "XPPPOPPPX" + "OXOXOXOXO");
```


##permissions
users need permission to use buttons:

permission                                           | description
-----------------------------------------------------| -------------
**inventorygui.button.bungee.teleport.<servername>** | permission to use the bungee teleport button to the server with <servername>
**inventorygui.button.command** | permission to use buttons with command. **Note:** A user can use the buttons command also he isn't allowed to use the command normally in chat
**inventorygui.button.teleport.<worldname>** | permission to use the teleport button to a location with the given <worldname>


##Configure via [itemset](https://bitbucket.org/Tantalos/itemset)

This plugin supports an easy configuration for a gui via the [itemset](https://bitbucket.org/Tantalos/itemset) plugin. For furhter information and configuration options see the [itemset wiki](https://bitbucket.org/Tantalos/itemset/wiki/Home).

The inventory gui is a item container with 2 optional arguments. Use the `InventoryGuiFactory` for Gui dialog and `ButtonGuiFacotry` for the buttons.
Buttons are configured like items, but they take mandatory arguments, which depends on the button type. For details see the example below. 



```
#!XML
<itemset>
<item-definition>
    <item-container id="inventoryGui" name="Inventory" item-factory="de.tantalos.inventorygui.InventoryGuiFactory">
        <arg>pattern=PPXOXXPPP</arg>
        <arg>material=COMPASS</arg>
        <item-slot item="button1" position="0" />
        <item-slot item="button2" position="0" />
        <item-slot item="button3" position="0" />
    </item-container>

    <item id="button1" name="Button1" item-factory="de.tantalos.inventorygui.ButtonGuiFactory">
        <arg>type=teleport</arg>
        <arg>material=BLAZE_ROD</arg>
        <arg>world=world</arg>
        <arg>x=0</arg>
        <arg>y=80</arg>
        <arg>z=0</arg>
    </item>

    <item id="button2" name="Button2" item-factory="de.tantalos.inventorygui.ButtonGuiFactory">
        <arg>type=bungeeTeleport</arg>
        <arg>material=IRON_SWORD</arg>
        <arg>server=spawn</arg>
    </item>

    <item id="button3" name="Button3" item-factory="de.tantalos.inventorygui.ButtonGuiFactory">
        <arg>type=teleport</arg>
        <arg>material=STICK</arg>
        <arg>world=world</arg>
        <arg>x=-5</arg>
        <arg>y=80</arg>
        <arg>z=-5</arg>
    </item>
</item-definition>
</itemset>

```

Use the `/itemset give <item id> [player name]` command to give the gui item. 

For material add the enum from [spigot material](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html) (e.g material=COMPASS, material=BONE etc.)