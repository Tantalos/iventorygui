## [0.0.5-SNAPSHOT] - 2017-03-30
### Fixed
- fixed bug: When several gui items were created they mixed up their behaviour
- fixed bug: When a world manager is use the world was sometimes not loaded before used

### Added
- Added a "nothing" button.
- Arrangement pattern can now be separated by space " " (allow more readable configuration)
- Added itemset options for buttons and gui item (name, lore)

### Changed
- removed itemset option 'itemTextColor'. Color can be set with color code


## [0.0.4-SNAPSHOT] - 2017-03-10
### Changed
- repository for dependencies


## Before
- Added support for itemset
- Added buttons for teleport, bungeteleport and command